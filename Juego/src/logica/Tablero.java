package logica;

import java.util.Random;

public class Tablero{
	
	private boolean[][] tabla;
	private Integer filas;
	private Integer columnas;
	
	public Tablero(Integer _filas, Integer _columnas) {
		filas = _filas;
		columnas = _columnas;
		tabla = new boolean[filas][columnas];
		
		for(int i = 0; i < tabla.length; i++) {
			for(int j = 0; j < tabla[0].length; j++) {
				tabla[i][j] = false;
			}
		}
	}
	
	public void randomnizable() {
		Random r = new Random();
		for(int i = 0; i < tabla.length; i++) {
			cambiarValorCasilla(r.nextInt(filas), r.nextInt(columnas));
		}
	}
	

	public boolean estaEncendido(int i, int j) {
		return tabla[i][j];
	}
	
	public void cambiarValorCasilla(int i, int j) {
		tabla[i][j] = !tabla[i][j];
		if(i > 0){ 
			tabla[i-1][j] = !(tabla[i-1][j]);
		}
		if(i < filas-1){ 
			tabla[i+1][j] = !(tabla[i+1][j]);
		}
		if(j > 0){ 
			tabla[i][j-1] = !(tabla[i][j-1]);
		}
		if(j < columnas-1){ 
			tabla[i][j+1] = !(tabla[i][j+1]);
		}
	}
	
	public boolean ganasteJuego() {
		boolean tableroResuelto = true;
		for(int i = 0; i < tabla.length; i++) {
			for(int j = 0; j < tabla[0].length; j++) {
				tableroResuelto = tableroResuelto && (tabla[i][j] == false);
			}
		}
		return tableroResuelto;
	}
	


	public Integer getFilas() {
		return filas;
	}


	public Integer getColumnas() {
		return columnas;
	}
	
	public void setValor(boolean b, int i, int j) {
		tabla[i][j] = b;
	}
	

}
