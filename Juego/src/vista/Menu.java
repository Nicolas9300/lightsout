package vista;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Color;
import javax.swing.ImageIcon;


import audio.SClip;

import javax.swing.JButton;

public class Menu implements ActionListener{

	private JFrame frame;
	private JButton btnStart;
	private SClip sound;

	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Menu window = new Menu();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
	public Menu() {
		
		initialize();
	}

	
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setForeground(Color.WHITE);
		frame.getContentPane().setBackground(Color.BLACK);
		frame.setBounds(100, 100, 600, 681);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		
		sound = new SClip("src\\audio\\008651989_prev.wav");
		
		JLabel lblNewLabel = new JLabel("Lights");
		lblNewLabel.setForeground(Color.YELLOW);
		lblNewLabel.setFont(new Font("Stencil", Font.PLAIN, 50));
		lblNewLabel.setBounds(147, 52, 174, 70);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblOut = new JLabel("OUT");
		lblOut.setForeground(Color.WHITE);
		lblOut.setFont(new Font("Stencil", Font.PLAIN, 60));
		lblOut.setBounds(314, 50, 174, 70);
		frame.getContentPane().add(lblOut);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setIcon(new ImageIcon("src/imagenes/lampara.png"));
		lblNewLabel_1.setBounds(181, 185, 210, 217);
		frame.getContentPane().add(lblNewLabel_1);
		
	    btnStart = new JButton("START");
		btnStart.setForeground(Color.BLACK);
		btnStart.setFont(new Font("Swiss921 BT", Font.PLAIN, 40));
		btnStart.setBackground(Color.YELLOW);
		btnStart.setBounds(194, 453, 186, 80);
		btnStart.addActionListener((ActionListener) this);
		frame.getContentPane().add(btnStart);
		
		
		
	}
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == btnStart) {
			sound.play();
			StartGame juego = new StartGame();
			juego.visible(true);
			frame.setVisible(false);
		}
	
	
	}
}
