package vista;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Color;
import javax.swing.ImageIcon;
import javax.swing.JButton;

public class Ganaste implements ActionListener{

	private JFrame frame;
    private JButton NewGame;
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ganaste window = new Ganaste();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
	public Ganaste() {
		initialize();
	}

	
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.YELLOW);
		frame.setBounds(100, 100, 453, 701);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		
		JLabel lblNewLabel = new JLabel("GANASTE!!");
		lblNewLabel.setFont(new Font("Trajan Pro 3", Font.BOLD, 60));
		lblNewLabel.setBounds(36, 55, 379, 93);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 5));
		lblNewLabel_1.setIcon(new ImageIcon("src/imagenes/confeti.png"));
		lblNewLabel_1.setBounds(89, 159, 256, 299);
		frame.getContentPane().add(lblNewLabel_1);
		
	    NewGame = new JButton("Nuevo Juego");
		NewGame.setFont(new Font("Tahoma", Font.PLAIN, 20));
		NewGame.setForeground(new Color(255, 215, 0));
		NewGame.setBackground(new Color(0, 0, 0));
		NewGame.setBounds(128, 511, 170, 67);
		NewGame.addActionListener((ActionListener) this);
		frame.getContentPane().add(NewGame);
	}

	public void visible(boolean b) {
		frame.setVisible(b);
	}
	
	public void actionPerformed(ActionEvent e){
		if(e.getSource() == NewGame) {
			StartGame juego = new StartGame();
			juego.visible(true);
			frame.setVisible(false);
	    }
    }
}
