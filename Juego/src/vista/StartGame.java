package vista;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JPanel;

import javax.swing.border.LineBorder;

import audio.SClip;
import logica.Tablero;

import javax.swing.JButton;
import java.awt.Font;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class StartGame implements ActionListener {

	private JFrame frame;
	private JPanel panel;
	private JButton[][] panel_botones;
	private Tablero tab;
	private SClip music, sound, soundVictory;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					StartGame window = new StartGame();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public StartGame() {
		initialize();
	}

	private void initialize() {
		tab = new Tablero(4, 4);
		tab.randomnizable();
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.BLACK);
		frame.setBounds(100, 100, 901, 721);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.getContentPane().setLayout(null);
		frame.setLocationRelativeTo(null);

		music = new SClip("src\\audio\\melodyloops-freemium-feel-my-energy-2m0s.wav");
		sound = new SClip("src\\audio\\008651989_prev.wav");
		soundVictory = new SClip("src\\audio\\DRKGYPE-victory.wav");

		music.loop();

		panel_botones = new JButton[tab.getFilas()][tab.getColumnas()];
		panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(128, 128, 128), 3, true));
		panel.setBackground(Color.BLACK);
		panel.setBounds(145, 34, 618, 549);
		frame.getContentPane().add(panel);
		panel.setLayout(null);

		iniciarBotones();

		JLabel lblNewLabel = new JLabel("Lights");
		lblNewLabel.setForeground(Color.YELLOW);
		lblNewLabel.setFont(new Font("Stencil", Font.PLAIN, 50));
		lblNewLabel.setBounds(307, 611, 174, 70);
		frame.getContentPane().add(lblNewLabel);

		JLabel lblOut = new JLabel("OUT");
		lblOut.setForeground(Color.WHITE);
		lblOut.setFont(new Font("Stencil", Font.PLAIN, 60));
		lblOut.setBounds(471, 609, 174, 70);
		frame.getContentPane().add(lblOut);

		prenderApagarLuces();
	}

	public void visible(boolean b) {
		frame.setVisible(b);

	}

	public void iniciarBotones() {
		int posX = 43; 
		int posY = 42; 
		for (int i = 0; i < panel_botones.length; i++) {
			for (int j = 0; j < panel_botones[0].length; j++) {
				panel_botones[i][j] = new JButton();
				panel_botones[i][j].setBackground(Color.YELLOW);
				panel_botones[i][j].setBounds(posX, posY, 109, 73);
				panel_botones[i][j].addActionListener((ActionListener) this);
				panel.add(panel_botones[i][j]);
				posX = posX + 145;
			}
			posX = 43;
			posY = posY + 121;
		}
	}

	public void actionPerformed(ActionEvent e) {
		for (int i = 0; i < panel_botones.length; i++) {
			for (int j = 0; j < panel_botones[0].length; j++) {
				if (e.getSource() == panel_botones[i][j]) {
					sound.play();
					tab.cambiarValorCasilla(i, j);
					prenderApagarLuces();
					if(tab.ganasteJuego()) {
						music.stop();
						soundVictory.play();
						Ganaste juego = new Ganaste();
						juego.visible(true);
						frame.setVisible(false);
					}
				}
			}
		}
	}

	public void prenderApagarLuces() {
		for (int i = 0; i < panel_botones.length; i++) {
			for (int j = 0; j < panel_botones[0].length; j++) {
				if (tab.estaEncendido(i, j) == true) {
					panel_botones[i][j].setBackground(new Color(255, 240, 0)); //Amarillo encendido
				} else {
					panel_botones[i][j].setBackground(new Color(97, 97, 95)); // Gris apagado
				}
			}
		}
	}

}
